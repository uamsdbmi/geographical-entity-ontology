An ontology of geographical entities implemented in Web Ontology Language 2 (OWL2) and based on Basic Formal Ontology (BFO). The developers aim to adhere to the OBO Foundry principles.

The issue tracker for this project is located [here](https://ontology.atlassian.net/browse/GEO).